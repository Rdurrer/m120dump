﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using m120Phonebook.Model;


namespace m120Phonebook.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        public ObservableCollection<Person> PersonList { get; set; } = new ObservableCollection<Person>();
        public string _firstname = "";
        public string _lastname = "";
        public string Firstname 
        { 
            get { return _firstname; }
            set
            {
                _firstname = value;
                OnPropertyChanged();
                OnPropertyChanged("Fullname");
            }
        }
        public string Lastname
        {
            get { return _lastname; }
            set
            {
                _lastname = value;
                OnPropertyChanged();
                OnPropertyChanged("Fullname");
            }
        }
        public BaseCommand ShowFullname { get; }

        public MainWindowViewModel() => ShowFullname =  new BaseCommand(Execute);

        private void Execute()
        {
            Person person = new Person()
            {
                Firstname = _firstname,
                Lastname = _lastname
            };
            PersonList.Add(person);
        }
    }
}
