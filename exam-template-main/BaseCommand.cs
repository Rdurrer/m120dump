﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     BaseCommand.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

#region using

using System;
using System.Windows.Input;

#endregion

#nullable enable

namespace Exam_Template
{
    public class BaseCommand : ICommand
    {
        private readonly Action<object?> _execute;
        private readonly Func<object?, bool>? _canExecute;

        public BaseCommand(Action<object?> execute, Func<object?, bool>? canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public BaseCommand(Action execute, Func<object?, bool>? canExecute = null) : this(_ => execute(), canExecute) { }

        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object? parameter) => _canExecute is null || _canExecute(parameter);

        public void Execute(object? parameter) => _execute(parameter);
    }
}
