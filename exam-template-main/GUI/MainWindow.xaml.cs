﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     MainWindow.xaml.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

#region using

#endregion

#region using

#endregion

namespace Exam_Template.GUI
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
    }
}
