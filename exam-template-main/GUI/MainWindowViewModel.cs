﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     MainWindowViewModel.cs
// 
// Last clean-up run on: 11.06.2021

#endregion

#region using

using System;
using System.ComponentModel;
using System.Windows;
using Exam_Template.Models;

#endregion

namespace Exam_Template.GUI
{
    public class MainWindowViewModel : ViewModelBase
    {
        private int _currentValue = default;

        public string Decimal
        {
            set {
                _currentValue = Convert.ToInt32(value);
                OnPropertyChanged(string.Empty);
            }
            get => _currentValue.ToString();
        }
        public string Hexadecimal
        {
            set {
                _currentValue = Convert.ToInt32(value, 16);
                OnPropertyChanged(string.Empty);
            }
            get => Convert.ToString(_currentValue, 16);
        }
        public string Octal
        {
            set {
                _currentValue = Convert.ToInt32(value, 8);
                OnPropertyChanged(string.Empty);
            }
            get => Convert.ToString(_currentValue, 8);
        }
        public string Binary
        {
            set {
                _currentValue = Convert.ToInt32(value, 2);
                OnPropertyChanged(string.Empty);
            }
            get => Convert.ToString(_currentValue, 2);
        }

        public BaseCommand TemplateCommand { get; }

        public MainWindowViewModel() => TemplateCommand = new(Execute);

        private void Execute() => MessageBox.Show("TestText");
    }
}
