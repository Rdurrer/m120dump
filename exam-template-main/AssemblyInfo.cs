#region Header

// This Project was made by Philipp W�rsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     AssemblyInfo.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

#region using

using System.Windows;

#endregion

[assembly: ThemeInfo(
    ResourceDictionaryLocation.None, //where theme specific resource dictionaries are located
    //(used if a resource is not found in the page,
    // or application resource dictionaries)
    ResourceDictionaryLocation.SourceAssembly //where the generic resource dictionary is located
    //(used if a resource is not found in the page,
    // app, or any theme specific resource dictionaries)
)]
