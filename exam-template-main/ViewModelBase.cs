﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     ViewModelBase.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

#region using

using System.ComponentModel;
using System.Runtime.CompilerServices;

#endregion

#nullable enable

namespace Exam_Template
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new(propertyName));
    }
}
