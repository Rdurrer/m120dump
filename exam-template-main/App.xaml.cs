﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     App.xaml.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

#region using

using System.Windows;

#endregion

namespace Exam_Template
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application { }
}
