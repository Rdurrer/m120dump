﻿#region Header

// This Project was made by Philipp Würsch
// Solution: Exam Template
// 
// Project:  Exam Template
// File:     Person.cs
// 
// Last clean-up run on: 10.06.2021

#endregion

namespace Exam_Template.Models
{
    public record Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
