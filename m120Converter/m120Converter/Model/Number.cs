﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace m120Converter.Model
{
    public class Number
    {
        public string Decimal { get; set; }
        public string Hexadecimal { get; set; }
        public string Binary { get; set; }
        public string Octal { get; set; }
    }
}
