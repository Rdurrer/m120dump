﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using m120Converter.Model;

namespace m120Converter.ViewModel
{
    public class NumberViewModel : ViewModelBase
    {

        //private Number _number = new Number(); 
        private int currentValue = default;
        private Random rnd = new Random();
        public string Decimal
        {
            set
            {
                currentValue = Convert.ToInt32(value);
                OnPropertyChanged("");
            }
            get { return currentValue.ToString(); }
        }
        public string Hexadecimal
        {
            set
            {
                currentValue = Convert.ToInt32(value, 16);
                OnPropertyChanged("");
            }
            get { return Convert.ToString(currentValue, 16); }
        }
        public string Octal
        {
            set
            {
                currentValue = Convert.ToInt32(value, 8);
                OnPropertyChanged("");
            }
            get { return Convert.ToString(currentValue, 8); }
        }
        public string Binary
        {
            set
            {
                currentValue = Convert.ToInt32(value, 2);
                OnPropertyChanged("");
            }
            get { return Convert.ToString(currentValue, 2); }
        }


        //public void ConvertDecimal()
        //{
        //    this._hexadecimal = rnd.Next(0, 1000);
        //    this._binary = rnd.Next(0, 1000);
        //    this._octal = rnd.Next(0, 1000);
        //}
        //public void ConvertBinary()
        //{
        //    this._hexadecimal = rnd.Next(0, 10000);
        //    this._decimal = rnd.Next(0, 10000);
        //    this._octal = rnd.Next(0, 10000);
        //}
        //public void ConvertHexadecimal()
        //{
        //    this._binary = rnd.Next(0, 10000);
        //    this._decimal = rnd.Next(0, 10000);
        //    this._octal = rnd.Next(0, 10000);
        //}
        //public void ConvertOctal()
        //{
        //    this._binary = rnd.Next(0, 10000);
        //    this._decimal = rnd.Next(0, 10000);
        //    this._hexadecimal = rnd.Next(0, 10000);
        //}
    }
}
