﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Input;

//namespace m120Converter
//{
//    public class BaseCommand : ICommand
//    {
//        private readonly Action<object?> _execute;
//        private readonly Func<object?, bool>? _canExecute;

//        public BaseCommand(Action<object?> execute, Func<object?, bool>? canExecute = null)
//        {
//            _execute = execute;
//            _canExecute = canExecute;
//        }

//        public BaseCommand(Action execute, Func<object?, bool>? canExecute = null) : this(_ => execute(), canExecute) { }

//        public event EventHandler? CanExecuteChanged;

//        public bool CanExecute(object? parameter) => _canExecute is null || _canExecute(parameter);

//        public void Execute(object? parameter) => _execute(parameter);
//    }
//}

